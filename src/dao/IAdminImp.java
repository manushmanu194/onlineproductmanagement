package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.Admin;
import com.model.Product;
import com.util.Db;
import com.util.Query;

public class IAdminImp implements AdminDao {
	PreparedStatement pst;
	ResultSet rs;
	int result;

	@Override
	public int adminAuthentication(Admin admin) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.adminAuth);
			pst.setString(1, admin.getUname());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in admin Authentication");

		} finally {
			try {
				pst.close();
				rs.close();
				Db.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {

				System.out.println("error");
			}

		}
		return result;
	}

	@Override
	public List<Product> viewAllProduct() {
		List<Product> list = new ArrayList<Product>();
		try {
			pst = Db.getConnection().prepareStatement(Query.viewAll);
			rs = pst.executeQuery();
			while (rs.next()) {
				Product info = new Product(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4));
				list.add(info);
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception Occurs in View All Products");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
				rs.close();

			} catch (ClassNotFoundException | SQLException e) {
			}
		}
		return list;
	}

	@Override
	public int addProduct(Product pr) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.addProduct);
			pst.setString(1, pr.getPname());
			pst.setString(2, pr.getPtype());
			pst.setInt(3, pr.getPprice());
			pst.setInt(4, pr.getId());

			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Excption occurs in the add developer");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
			}
		}
		return result;
	}

	@Override
	public int editType(Product pr) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editType);
			pst.setString(1, pr.getPtype());
			pst.setInt(2, pr.getId());
			result = pst.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception Occurs in Product type");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return result;

	}

	@Override
	public int editNamePrice(Product pr) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editNamePrice);
			pst.setString(1, pr.getPname());
			pst.setInt(2, pr.getPprice());
			pst.setInt(3, pr.getId());
			result = pst.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			//System.out.println("Exception Occurs in Product type");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return result;
	}

	@Override
	public int editNameTypePrice(Product pr) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editNameTypePrice);
			pst.setString(1, pr.getPname());
			pst.setString(2, pr.getPtype());
			pst.setInt(3, pr.getPprice());
			pst.setInt(4, pr.getId());
			result = pst.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception Occurs in Product type");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return result;
	}

	@Override
	public int removeProduct(Product pr) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.removeProduct);
			pst.setInt(1, pr.getId());
			result = pst.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception Occurs in Remove Developes");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return result;

	}
}
