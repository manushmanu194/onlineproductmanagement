package dao;

import java.util.List;

import com.model.Admin;
import com.model.Product;

public interface AdminDao {
public int adminAuthentication(Admin admin);
public List<Product> viewAllProduct();
public int addProduct(Product pr);
public int editType(Product pr);

public int editNamePrice(Product pr);

public int editNameTypePrice(Product pr);

public int removeProduct(Product pr);;
}
