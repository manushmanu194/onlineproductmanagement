package com.view;

import java.util.List;
import java.util.Scanner;

import com.controller.AdminController;
import com.model.Product;

public class Main {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Enter user name");
		String uname=s.nextLine();
		System.out.println("Enter Password");
		String password=s.nextLine();
		AdminController controller=new AdminController();
		int result=0;
		
	result=controller.adminAuthentication(uname, password);
		if(result>0) {
			System.out.println("welcome to admin" + " "+uname);
			int cont=0;
			do {
			
			System.out.println("1) Add new product  2) Remove product 3) Edit product  4) View All products");
			 int option = s.nextInt();
		        int id=0;
		        String pname="";
		        String ptype="";
		        int pprice = 0;
		        s.nextLine();
		        
		        switch(option) {
		        case 1:
		        	System.out.println("Enter  productname");
		        	 pname=s.nextLine();
		        	System.out.println("Enter Product type");
		        	 ptype=s.nextLine();
		        	System.out.println("Enter product price");
		        	 pprice=s.nextInt();
		        	System.out.println("Enter Product Id");
		        	 id=s.nextInt();
		        	result=controller.addProduct(pname, ptype, pprice, id);
		        	if(result>0) {
		        		System.out.println(id +"Added Succesfully");
		        	}
		        		
		        	break;
		        case 2:
		        	System.out.println("Enter Product Id to Remve");
		        	id=s.nextInt();
		        	result=controller.removeProduct(id);
	        		 System.out.println((result>0)?id + "Remove sucess":id+"Remove was not success");

		        	
		        	
		        	
		        	
		        	break;
		        case 3:
		        	System.out.println("1) Edit producttype 2) Edit product Name & price 3)Edit product Name Type Price");
		        	 option =s.nextInt();
		        	 if(option==1) {
		        		 System.out.println("Enter Product id");
		        		 id=s.nextInt();
		        		 s.nextLine();
		        		 System.out.println("Enter Product Type");
		        		 ptype=s.nextLine();
		        		 result = controller.editType(id, ptype);
		        		 System.out.println((result>0)?id + "Updated sucess":id+"Update was not success");
		        	 }else if(option==2) {
		        		 System.out.println("Enter Product id");
		        		 id=s.nextInt();
		        		 s.nextLine();
		        		 System.out.println("Enter Product Name");
		        		 pname=s.nextLine();
		        		 System.out.println("Enter Product price");
		        		 pprice=s.nextInt();
		        		 result = controller.editNamePrice(id, pname, pprice);
				        		 System.out.println((result>0)?id + "Updated sucess":id+"Update was not success");

		        	 }else if(option==3) {
		        		 System.out.println("Enter Product id");
		        		 id=s.nextInt();
		        		 s.nextLine();
		        		 System.out.println("Enter Product Name");
		        		 pname=s.nextLine();
		        		 System.out.println("Enter Product Type");
		        		 ptype=s.nextLine();
		        		 System.out.println("Enter Product price");
		        		 pprice=s.nextInt();
		        		 result = controller.editNameTypePrice(ptype, pname, id, pprice);
				        		 System.out.println((result>0)?id + "Updated sucess":id+"Update was not success");

		        		 
		        	 }else {
		        		 System.out.println("invalid input");
		        	 }
		        	break; 
		        case 4:
		        	 List<Product> list = controller.viewAllProduct();
		        	if(list.size()>0) {
		        	System.out.println("pname,ptype,price,id");	
		        	
		        	for (Product p : list) {
		        		System.out.println(p.getPname()+","+p.getPtype()+","+p.getPprice()+","+p.getId());
		        	}
		        		
		        	}else {
		        		System.out.println("No records found");
		        	}
		        	break;
		        	default:
		        		System.out.println("invalid Selection");
		        }
			System.out.println("press 1 to cotinue");
			cont=s.nextInt();
		}while(cont==1);
		}else {
			System.out.println("user id or password incorrect");
		}
		System.out.println("Done");
		s.close();
	}

}
