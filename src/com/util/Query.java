package com.util;

public class Query {
public static String adminAuth= "select * from admin where 	uname=? and password=? ";
public static String viewAll ="select * from product";
public static String addProduct="insert into product values(?,?,?,?)";
public static String editType="update product set ptype=? where id=?";
public static String editNamePrice="update product set pname=?,pprice=? where id =?";
public static String editNameTypePrice="update product set pname=?,ptype=?,pprice=? where id =?";
public static String removeProduct="delete from product where id=?";
}
