package com.controller;

import java.util.List;

import com.model.Admin;
import com.model.Product;

import dao.AdminDao;
import dao.IAdminImp;
public class AdminController {
int result;
 AdminDao dao= new IAdminImp();
 
	public int adminAuthentication(String uname,String password) {
		Admin admin=new Admin(uname,password);
		return dao.adminAuthentication(admin);
	}
	public List<Product> viewAllProduct(){
	return dao.viewAllProduct();	
	}
	public int addProduct(String pname,String ptype,int pprice,int id) {
		Product pr =new Product(pname,ptype,pprice,id);
		return dao.addProduct(pr);
	}
	public int editType(int id ,String Ptype) {
		Product pr =new Product();
		pr.setId(id);
		pr.setPtype(Ptype);
		return dao.editType(pr); 
	}
	public int editNamePrice(int id, String pname,int pprice) {
		Product pr =new Product();
		pr.setId(id);
		pr.setPname(pname);
		pr.setPprice(pprice);
		return dao.editNamePrice(pr);
		
	}
	public int editNameTypePrice(String ptype,String pname,int id,int pprice) {
		Product pr =new Product(pname,ptype,pprice,id);
		
		
		return dao.editNameTypePrice(pr);
		
	}
	public int removeProduct(int id) {
		Product pr=new Product();
		pr.setId(id);
		return dao.removeProduct(pr);
		
	}
}
